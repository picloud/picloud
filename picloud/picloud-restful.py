import json
import os
import random
import requests
import yaml

from flask import Flask, request, redirect, jsonify, app
from flask_restful import Resource, Api

from fabric.context_managers import settings
from fabfile import deploy, kill_process


app_info = {}
fab_details = {}

def host_list():
    slaves = []
    fname = "/home/ubuntu/picloud/config.yaml"
    with open(fname) as f:
        list_doc = yaml.load(f)
        if list_doc["slaves"]:
            slaves = list_doc["slaves"]
    return slaves


def get_active_serving_hosts(deployed_hosts):
    all_hosts = host_list()
    serving_hosts = list(set(all_hosts).intersection(deployed_hosts))
    return serving_hosts


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        rv['status_code'] = self.status_code
        return rv


class App(object):
    MANUAL = 'manual'
    AUTO = 'auto'
    def __init__(self, **kwargs):
        try:
            self.app_name = kwargs['app_name']
            self.number_of_replicas = kwargs['number_of_replicas']
            self.port = kwargs['port']
            self.git = kwargs['git']
            self.app_type = kwargs.get('type', 'python')
            self.redeploy_type = kwargs.get('redeploy_type', App.MANUAL)

            # Metadata about the app's existence on hosts, like pid, etc
            self.metadata = {
                'serving_hosts': [],
                'status_line': "",
                'pids': [],
                'last_served_from': 0,
                'ping_over': False,
                'webhook_id': 0,
            }
        except KeyError as k:
            raise InvalidUsage("Did not get a value for {0}".format(k.message), status_code=406)

    def json_helper(self):
        self.metadata['status_line'] = '{0}/{1} replicas in service'.format(
            len(self.metadata['serving_hosts']), self.number_of_replicas)
        return json.loads(json.dumps(self, default=lambda o: o.__dict__, 
                          sort_keys=True, indent=4))


class Application(Resource):

    def update_serving_hosts(self, app):
        serving_hosts = get_active_serving_hosts(app.metadata['serving_hosts'])
        new_serving_hosts, new_pids = [], []
        for i in range(len(app.metadata['serving_hosts'])):
            if app.metadata['serving_hosts'][i] in serving_hosts:
                new_serving_hosts.append(app.metadata['serving_hosts'][i])
                new_pids.append(app.metadata['pids'][i])
        app.metadata['serving_hosts'] = new_serving_hosts
        app.metadata['pids'] = new_pids
        return app

    def get(self, app_name):
        # The user wants to GET an app, that is see what nodes it is running
        # on, etc. Eg <master_ip:80/hello_app> should return info about the app
        # called hello_app
        if app_name not in app_info:
            return "No app named {0}".format(app_name)

        # Update serving hosts list
        app = self.update_serving_hosts(app_info[app_name])
        app_info[app_name] = app
        return {app_name: app_info[app_name].json_helper()}

    def delete(self, app_name):
        # The user wants to delete an app, that is to stop the app on all the
        # nodes it is running on.
        if app_name not in app_info:
            return "No app named {0}".format(app_name)

        app = self.update_serving_hosts(app_info.pop(app_name))
        print "fab_details: ", fab_details
        for i in range(len(app.metadata['serving_hosts'])):
            serving_host_name = app.metadata['serving_hosts'][i]
            host_string = fab_details['fab_user'] + '@' + app.metadata['serving_hosts'][i]
            try:
                with settings(host_string=host_string):
                    result = kill_process(app.metadata['pids'][i])
            except:
                pass

        # Delete webhook
        if app.redeploy_type == App.AUTO and app.metadata['webhook_id']:
            app_user = app.git.split('/')[3]
            app_repo_name = app.git.split('/')[4]
            url='https://api.github.com/repos/{0}/{1}/hooks/{2}'.format(
                app_user, app_repo_name, app.metadata['webhook_id'])
            auth = (fab_details['git_user'], fab_details['git_password'])
            try:
                r = requests.delete(url, auth=auth, timeout=4)
            except requests.exceptions.Timeout:
                print "Cannot connect to github"
        return {app_name: app.json_helper()}

    def restart_app(self, app):
        print "Redeploying", app.app_name
        # First kill the app
        for i in range(len(app.metadata['serving_hosts'])):
            host_string = fab_details['fab_user'] + '@' + app.metadata['serving_hosts'][i]
            try:
                with settings(host_string=host_string):
                    result = kill_process(app.metadata['pids'][i])
            except:
                pass

        # Then deploy again
        arr = host_list()
        random.shuffle(arr)
        target_hosts = arr[0:app.number_of_replicas]

        # Reset some of the metadata
        app.metadata['serving_hosts'] = []
        app.metadata['pids'] = []

        for host in target_hosts:
            host_string = fab_details['fab_user'] + '@' + host
            with settings(host_string=host_string):
                result = deploy(app.git, app.port)
                pid = result[result.keys()[0]]
                print "New app started on: {0} as pid", pid

            app.metadata['serving_hosts'].append(host)
            app.metadata['pids'].append(pid)

    def put(self, app_name):
        if app_name not in app_info:
            return "No app named {0}".format(app_name)
        app = app_info[app_name]
        app = self.update_serving_hosts(app_info[app_name])
        app_info[app_name] = app

        self.restart_app(app)
        return {app_name: app.json_helper()}

    def post(self, app_name):
        print "received webhook request for ", app_name
        if app_name not in app_info:
            return 
        app = app_info[app_name]
        if app.metadata['ping_over']:
            self.restart_app(app)
        else:
            app.metadata['ping_over'] = True
        return {app_name: app.json_helper()}

class PiCloud(Resource):

    def handle_invalid_usage(self, error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response

    def post(self):
        data = request.get_json(force=True)
        if data['app_name'] in app_info:
            return {'msg': 'Duplicate app, use another name'}

        if data['number_of_replicas'] > len(host_list()):
            return {'msg': 'Sorry, not enough nodes to service your request!'}

        # TODO:
        # 1. Store these configs in app_info
        try:
            new_app = App(**data)
        except InvalidUsage as e:
            return self.handle_invalid_usage(e)

        app_info[data['app_name']] = new_app
        # 2. Deploy (using fabric)

        print "Before deploying: "
        print "fab_details: ", fab_details

        # pick some hosts from host_list
        arr = host_list()
        random.shuffle(arr)
        target_hosts = arr[0:new_app.number_of_replicas]

        for host in target_hosts:
            host_string = fab_details['fab_user'] + '@' + host
            with settings(host_string=host_string):
                result = deploy(new_app.git, new_app.port)
                pid = result[result.keys()[0]]
                print "New app started on: {0} as pid", pid
            new_app.metadata['serving_hosts'].append(host)
            new_app.metadata['pids'].append(pid)

        if new_app.redeploy_type == App.AUTO:
            # register new githook
            # https://github.com/sandhya123r/helloworld
            app_user = new_app.git.split('/')[3]
            app_repo_name = new_app.git.split('/')[4]
            url='https://api.github.com/repos/{0}/{1}/hooks'.format(app_user, app_repo_name)
            auth = (fab_details['git_user'], fab_details['git_password'])
            hook_data = {
                "name": "web",
                "active": True,
                "events": ["push"],
                "config": {
                    "url": "{0}:8080/picloud/applications/{1}".format(fab_details['master'], new_app.app_name),
                     "content_type": "json" 
                }
            }
            headers = {"content": "application/json"}

            print "*" * 100
            print url, headers, hook_data, auth
            try:
                r = requests.post(url, headers=headers, json=hook_data, auth=auth, timeout=5)
                print r.status_code, r.text
                if r.status_code == 201:
                    new_app.metadata['webhook_id'] = json.loads(r.text)['id']
            except requests.exceptions.Timeout:
                print "Cannot connect to github"

            print "*" * 100

        return {data['app_name']: data}


class LoadBalancer(Resource):

    def get(self, app_name):
        # In this function, we take the request from user for an app (say app_name),
        # make a request to one of the slaves running the app, and return
        # the return value.
        if app_name not in app_info:
            return "No app named {0}".format(app_name)
        # For the URL
        # 1. The hostname should be formed in a round robin fashion.
        # 2. The port should be picked up from app_info
        # 3. The request goes to the master, which is the load balancer. One of
        #    the  replicas should respond, and not the master
        serving_hosts = get_active_serving_hosts(app_info[app_name].metadata['serving_hosts'])
        serving_port = app_info[app_name].port 
        last_served_from = app_info[app_name].metadata['last_served_from']
        new_serve_index = (last_served_from + 1) % len(serving_hosts)
        chosen_host = serving_hosts[new_serve_index]

        app_info[app_name].metadata['last_served_from'] = new_serve_index

        url = "http://{0}:{1}/".format(chosen_host, str(serving_port))
        r = requests.get(url=url)
        result = json.loads(r.text)
        result['serving_host'] = chosen_host
        return result


class PiCloudHelp(Resource):
    def get(self):
        sample_request = {
        "app_name": "helloworld",
        "port": 5000,
        "git": "https://github.com/sandhya123r/helloworld",
        "type": "python",
        "redeploy_type": "manual",
        "number_of_replicas": 2,
        }
        return {'Request format': sample_request}


class Server(object):
    def __init__(self, config_file):
        with open(config_file, 'r') as cfg_file:
            configs = yaml.load(cfg_file)

        global fab_details
        fab_details['fab_user'] = configs['fab_user']
        fab_details['git_user'] = configs['git_user']
        fab_details['git_password'] = configs['git_password']
        try:
            r = requests.get('http://ip.42.pl/raw', timeout=10)
            fab_details['master'] = str(r.text)
        except requests.exceptions.Timeout:
            print "Cannot connect to address to grab public ip"
            fab_details['master'] = configs['master']

        self.app = Flask(__name__)
        self.api = Api(self.app)
        self.api.add_resource(Application, '/picloud/applications/<string:app_name>')
        self.api.add_resource(PiCloud, '/picloud')
        self.api.add_resource(PiCloudHelp, '/picloud/help')
        self.api.add_resource(LoadBalancer, '/<string:app_name>')


    def start(self):
        self.app.run(host="0.0.0.0", debug=True, port=8080)

if __name__ == '__main__':
    config_file = '/home/ubuntu/picloud/config.yaml'
    server = Server(config_file)
    server.start()
