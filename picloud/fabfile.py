from fabric.api import run,env,execute
import uuid
import yaml
fname = "/home/ubuntu/picloud/config.yaml"

# Given a git repo, create a new venv for it and start its exectuable
# TODO: We assume the exec is inside bin/ and is called run_this.py. Maybe
# make it more generic
def fetch_repo(repo, port=5000):
    dir_name = repo.split('/')[-1]
    executable = 'bin/run_this.py'
    venv_name = 'venv-{0}'.format(str(uuid.uuid4()))
    cmd = [
        'cd /tmp',
        'rm -rf {0}'.format(dir_name),
        'git clone {0}'.format(repo),
        'virtualenv {0}'.format(venv_name),
        'source {0}/bin/activate'.format(venv_name),
        'cd /tmp/{0}'.format(dir_name),
        'pip install -r requirements.txt',
        'python setup.py install',
        'nohup python {0} {1} &> /dev/null & echo "Started task on:  "$!'.format(executable, port),
    ]
    cmd = ';'.join(cmd)
    result = run(cmd, pty=False)
    return result.split("Started task on:",1)[1].strip()

def addnode(node_ip) :
    fname = "/home/ubuntu/picloud/config.yaml"
    with open(fname) as f:
        slave_dict = yaml.load(f)
        if not slave_dict["slaves"]:
            slave_dict["slaves"] = []
        slave_dict["slaves"].append(node_ip)
        with open(fname, "w") as f:
            yaml.dump(slave_dict,f, default_flow_style=False)


def removenode(node_ip) :
    with open(fname) as f:
        slave_dict = yaml.load(f)
        new_dict = slave_dict["slaves"]
        if node_ip not in new_dict:
            return
        new_dict.remove(node_ip)
        slave_dict["slaves"] = new_dict
        with open(fname, "w") as f:
            yaml.dump(slave_dict,f, default_flow_style=False)


def deploy(location, port):
   return execute(fetch_repo, location, port)

def kill_process(pid):
    cmd = [
        'kill {0}'.format(pid),
    ]
    cmd = ';'.join(cmd)
    result = run(cmd, pty=False)


def register_slave():
    cmd = [
        'sudo apt-get update',
        'sudo apt-get install -y git python-pip',
        'sudo pip install virtualenv',
    ]
    cmd = ';'.join(cmd)
    result = run(cmd)


def kill_process_on_slaves():
    cmd = [
        "ps aux | grep python | awk 'NR==1{print $2}'  | xargs kill"
    ]
    cmd = ';'.join(cmd)
    result = run(cmd, pty=False)
