from fabric.context_managers import env
from fabric.api import local,execute

def fetch_repo(some_str=None):
    print some_str
    repo = 'https://github.com/sandhya123r/helloworld'
    dir_name = repo.split('/')[-1]
    executable = 'bin/helloworld.py'
    cmd = [
        'git clone {0}'.format(repo),
        'cd {0}'.format(dir_name),
        'virtualenv venv',
        'virtualenv -p /usr/bin/python venv',
        'source venv/bin/activate',
        'pip install -r requirements.txt',
        'python {0}'.format(executable),
    ]
    local(';'.join(cmd))


def deploy(location="https://github.com/sandhya123r/helloworld"):
   print ("Deploying from %s!" % location)
   execute(fetch_repo, "test")
