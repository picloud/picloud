#Reference to take a git repo input and run the app executable
#Executable is hardcoded here, need to change that


from fabric.api import run,env,execute

env.hosts = [
        'localhost',
        'localhost',
        ]

def fetch_repo(some_str=None):
    print some_str
    repo = 'https://github.com/sandhya123r/helloworld'
    dir_name = repo.split('/')[-1]
    executable = 'bin/helloworld.py'
    cmd = [
        'git clone {0}'.format(repo),
        'cd {0}'.format(dir_name),
        'virtualenv venv',
        'virtualenv -p /usr/bin/python venv',
        'source venv/bin/activate',
        'pip install -r requirements.txt',
        'python {0}'.format(executable),
    ]
    run(';'.join(cmd))

def deploy(location="https://github.com/sandhya123r/helloworld"):
   print ("Deploying from %s!" % location)
   execute(fetch_repo, "test")
