import json

from flask import Flask, request, jsonify
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)


class Application(Resource):
    def get(self, app_name):
        # The user wants to GET an app, that is see what nodes it is running
        # on, etc. Eg <master_ip:80/hello_app> should return info about the app
        # called hello_app
        return {app_name: "received GET request"}

    def delete(self, app_name):
        # The user wants to delete an app, that is to stop the app on all the
        # nodes it is running on.
        return {app_name: "received delete request"}


class PiCloud(Resource):
    def post(self):
        # User wants to deploy a new app. Refer specs file for example.
        data = request.get_json(force=True)
        return {"key": "received post request"}
        # return {data['app_name']: data}


api.add_resource(Application, '/picloud/servers/<string:app_name>')
api.add_resource(PiCloud, '/picloud')

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
